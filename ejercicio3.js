
var letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N',
    'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];
function letra() {
    const numDNI = prompt("Introduzca el número de su DNI sin la letra");
    if (numDNI > 99999999 || numDNI < 0) {
        prompt("el número introducido no es correcto")
    } else {
        const letra = prompt("introduzca letra");
        if (letras[numDNI % 23] === letra.toUpperCase()) {
            alert("DNI válido");
        } else {
            alert("DNI incorrecto")
        }
    }
}